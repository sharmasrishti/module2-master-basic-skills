# Object Detection 

Object detection is a computer vision technique that allows us to identify and locate objects in an image or video. with the help of identifiction and localization technique we are able to count how many objects are present in a scene or an image or video.

![](https://miro.medium.com/max/1000/0*OAuVCw-k7XZOekBa)

### Working of object detection.

Input: An image with one or more objects, such as a photograph.

Output: One or more bounding boxes (e.g. defined by a point, width, and height), and a class label for each bounding box.

![](https://miro.medium.com/max/5712/1*GU-nroHGjzP-mqeFbidxdA.png)

### Uses of object Detection
- Sports Broadcasting 
- Video Collabration 
- AI and Machine Learning 

![](https://www.hoffmann-krippner.com/wp-content/uploads/2018/08/ultrasonic-sensor-standard-applications.png)

