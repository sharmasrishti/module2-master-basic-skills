# Face Detection

Face detection is a computer technology which is  used in a variety of applications that identifies human faces in digital images. Face detection also refers to the psychological process by which humans locate and attend to faces in a visual scene.

![](https://i.pinimg.com/originals/1c/e6/5b/1ce65b7e107dd768c306134afe35f03e.jpg)

### How does face detection works?

It uses biometrics to map facial features from a photograph or video. It compares the information with a database of known faces to find a match. Facial recognition can help verify personal identity, but it also raises privacy issues.

![](https://miro.medium.com/max/4658/1*sgROTW1Wa-u4hhUN62tWyw.png)

### Use of face detection

The process of fsce detection is an essential step as it detects and locates human faces in images and videos. The face capture process transforms analogue information (a face) into a set of digital information (data) based on the person's facial features.

![](https://docplayer.net/docs-images/65/53005521/images/50-0.jpg)