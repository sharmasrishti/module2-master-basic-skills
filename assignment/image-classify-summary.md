# Image Classification

Image classification refers to the task of extracting information classes from a multiband raster image.  to perform classification and multivariate analysis is through the Image Classification toolbar.

![](https://miro.medium.com/max/3200/1*Y_CXnEcdZRJoyBVHOdL3ng.png)

### Working of image classification

Image classification is the process of assigning land cover classes to pixels. For example, classes include water, urban, forest, agriculture and grassland.

The 3 main image classification techniques in remote sensing are:
- Unsupervised image classification.
- Supervised image classification.
- Object-based image analysis

![](https://miro.medium.com/max/2510/1*vkQ0hXDaQv57sALXAJquxA.jpeg)

### Use of image classification

![](https://slideplayer.com/slide/6230079/20/images/9/Applications+of+Image+Classification.jpg)
.